//fichero JavaScript

function insertarUsuari(){
  //En el cas de que els 5 TextBox estiguin buits
  if(formAfegir.nom.value=="" && formAfegir.cognom1.value=="" && formAfegir.cognom2.value=="" && formAfegir.datanaixament.value==""&& formAfegir.telefon.value=="" ){
   alert("Introdueix nom d'usuari, cognom1, cognom2, data de naixament i el telefon.");
  }
  //En el cas de que el TextBox d'usuari estigui buit
  else if(formAfegir.nom.value==""){
   alert("Introdueix un nom d'usuari.");
  }
  //En el cas de que el TextBox cognom1 estigui buit
  else if(formAfegir.cognom1.value==""){
   alert("Introdueix cognom1.");
  }
  //En el cas de que el TextBox cognom2 estigui buit
  else if(formAfegir.cognom2.value==""){
   alert("Introdueix cognom2.");
  }
  //En el cas de que el TextBox data naixament estigui buit
  else if(formAfegir.datanaixament.value==""){
   alert("Introdueix una data de naixament.");
  //En el cas de que el telefon estigui buit
  }else if(formAfegir.telefon.value==""){
   alert("Introdueix un telefon.");
  //si el valor introduit no es numeric és error
  }else if(!/^[0-9]+$/.test(formAfegir.telefon.value)){
   alert("El valor ha de ser numéric.");
  }else{
  document.forms.formAfegir.submit();
  }
}

function calendari(){
  //obrira una nova finestra del fitxer calendari.php amb les seves mides corresponents
  window.open("funcions/calendari.php","_blank","width=300,height=260");
}