var nMes = 0;
var mesos = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
var dataNeixament = new Date(new Date().getFullYear(), nMes, 1);
var anyActual = dataNeixament.getFullYear();

	function inicial(){
		if(document.getElementById("calendari"))
		{
			var nodeCalendari = document.getElementById("calendari");
			nodeCalendari.parentNode.removeChild(nodeCalendari);
		}
		else{
			crearCalendari();
		}
	}

	function crearCalendari() {
		var nodeEdat= document.getElementById("inp_tdEdat");
		var nodeCalendari = document.createElement("div");
		nodeCalendari.setAttribute("id", "calendari");
		nodeEdat.parentNode.insertBefore(nodeCalendari, nodeEdat);


		var nodeMesos = document.createElement("div");
		nodeMesos.setAttribute("id", "mesos");
		nodeCalendari.appendChild(nodeMesos);

		var nodeMes = document.createElement("div");
		nodeMes.setAttribute("id", "mes");
		nodeMesos.appendChild(nodeMes);

		var mesAnt = document.createElement("a");
		mesAnt.setAttribute("class", "mesAnt");
		mesAnt.setAttribute("onclick", "mesAnt()");
		nodeMes.appendChild(mesAnt);
		var textAnt = document.createTextNode("<<");
		mesAnt.appendChild(textAnt);

		var dataMes = document.createElement("p");
		dataMes.setAttribute("id", "dataMes");
		nodeMes.appendChild(dataMes);

		var mesSeg = document.createElement("a");
		mesSeg.setAttribute("class", "mesSeg");
		mesSeg.setAttribute("onclick", "mesSeg()");
		nodeMes.appendChild(mesSeg);
		var textSeg = document.createTextNode(">>");
		mesSeg.appendChild(textSeg);

		var nodeAny = document.createElement("input");
		nodeAny.setAttribute("type", "number");
		nodeAny.setAttribute("id", "any");
		nodeAny.setAttribute("min", "1900");
		nodeAny.setAttribute("max", anyActual);
		nodeAny.setAttribute("value", anyActual);
		nodeAny.setAttribute("onchange", "canviarAny()");
		nodeMesos.appendChild(nodeAny);

		var mes = document.getElementById("dataMes");
		mes.innerHTML = mesos[nMes];

		mostrarDies();
}

function canviarAny(){
	var nouAny = document.getElementById("any").value;
	dataNeixament = new Date(nouAny, nMes, 1);
	actualitzarCalendari();
}

function mostrarDies(){
	var nodeCalendari = document.getElementById("calendari");
	var nodeSetmanes = document.createElement("div");
	nodeSetmanes.setAttribute("id", "setmanes");
	nodeCalendari.appendChild(nodeSetmanes);
	var dMes = diesMes();
	var dSetmana = primerDiaMes();
	var totalDies = dMes + dSetmana;
	var nDies= 1;
	var i=1;
	while(i<=totalDies)
	{
		var nodeSetmana = document.createElement("setmana");
		nodeSetmana.setAttribute("id","setmana");
		nodeSetmanes.appendChild(nodeSetmana);
		for(j=1;j<=7;j++)
		{
			if(i > dSetmana && i <= totalDies)
			{
				var nodeDia = document.createElement("div");
				nodeDia.setAttribute("id", "dia");
				nodeSetmana.appendChild(nodeDia);
				var linkDia = document.createElement("a");
				linkDia.setAttribute("id", "linkDia");
				linkDia.setAttribute("onclick", "dataClick("+ nDies +")");
				nodeDia.appendChild(linkDia);
				var textDia = document.createTextNode(nDies);
				linkDia.appendChild(textDia);
				nDies++;
			}
			else{
				var nodeDia = document.createElement("div");
				nodeDia.setAttribute("id", "dia");
				nodeSetmana.appendChild(nodeDia);
				var textDia = document.createTextNode("");
				nodeDia.appendChild(textDia);
			}
			i++;
		}
	}
}

function actualitzarCalendari(){
	var nodeSetmanes = document.getElementById("setmanes");
	setmanes.parentNode.removeChild(nodeSetmanes);
	mostrarDies();
}

function mesAnt(){
	var mes = document.getElementById("dataMes");
	nMes--;
	if(nMes >= 0)
		mes.innerHTML = mesos[nMes];
	else {
		nMes=11;
		mes.innerHTML = mesos[nMes];
	}
	dataNeixament = new Date(new Date().getFullYear(), nMes, 1);
	actualitzarCalendari();
}

function mesSeg(){
	var mes = document.getElementById("dataMes");
	nMes++;
	if(nMes <= 11)
		mes.innerHTML = mesos[nMes];
	else{
	    nMes=0;
		mes.innerHTML = mesos[nMes];
	}
	dataNeixament = new Date(dataNeixament.getFullYear(), nMes, 1);
	actualitzarCalendari();
}

function diesMes() {
	tDies = dataNeixament;
	tDies = new Date(dataNeixament.getFullYear(), nMes+1, 0);
	var totalDies = tDies.getDate();
	return totalDies;
}

function primerDiaMes() {
	var diaSetmana = dataNeixament.getDay();
	return diaSetmana-1;
}
function dataClick(dia){
	var mostrarData= new Date(dataNeixament.getFullYear(), nMes,  dia);
	var dia = mostrarData.getDate();
	var mes = (mostrarData.getMonth() + 1);
	if(dia<10) {dia = "0" + dia;}
	if(mes<10) {mes = "0" + mes;}
	var formatData = dia + "/" + mes  +  "/" + mostrarData.getFullYear();
	var edat = document.getElementById("inp_tdEdat");
	edat.value = formatData;
	inicial();
}
