<?php
require_once('conexio.php');

/* variables sql*/
$taula = 'contactes';
$campBus = 'contact_nom';
$user_id = 0;
if (isset($_POST['user_id']))$user_id = $_POST['user_id'];
$select = "SELECT * FROM ".$taula." WHERE 'contact_ID' = ".$user_id;
$mysql = new mysqli(HOST, USER, PASSWD, DB);

/* conexión al servidor de base de datos */
if ($mysql->connect_error)
  die("Error al conectarse al servidor");
else{
  $result = $mysql->query($select);
  $num_contactes = $result->num_rows;

//Buscador

$url = basename($_SERVER ["PHP_SELF"])."?";
$cadena = "";
if(isset($_GET['searchbox'])) {
  //$_POST['searchbox'] = $_SESSION["buscar"];
  //$cadena = $_SESSION["buscar"];
  $cadena = $_GET['searchbox'];
  $select = "SELECT * FROM ".$taula."
  WHERE 'contact_ID' = ".$user_id." AND (contact_nom LIKE '%{$cadena}%'
  OR contact_cog1 LIKE '%{$cadena}%'
  OR contact_cog2 LIKE '%{$cadena}%'
  OR contact_tlf LIKE '%{$cadena}%'
  OR contact_edat LIKE '%{$cadena}%')
  ";
  //$_SESSION["buscar"]=$_POST['searchbox'];
  //header('Location: agenda.php');
$pag_actual=1;
//echo "url shearch: ".$url;
$url = $url."searchbox=".$_GET['searchbox']."&";
}


/* ordenacio */
$order = "contact_ID";
$orderASC = "ASC";
$orderBy = array('contact_ID', 'contact_nom', 'contact_cog1','contact_tlf', 'contact_edat');

if (isset($_GET['orderBy']) && $_GET['orderASC'] == "ASC" && in_array($_GET['orderBy'], $orderBy)) {
  $order = $_GET['orderBy'];
  $_GET['$orderASC'] = "DESC";
}
else if(isset($_GET['orderBy']) && $_GET['orderASC'] == "DESC" && in_array($_GET['orderBy'], $orderBy)) {
  $order = $_GET['orderBy'];
  $_GET['$orderASC'] = "ASC";
}
if( isset($_GET['$orderASC'])){
  if($_GET['$orderASC'] == "ASC" || $_GET['$orderASC'] == "DESC")
    $orderASC = $_GET['$orderASC'];

    $url = $url."orderBy=".$_GET['orderBy']."&orderASC=".$_GET['$orderASC']."&";

}


  /* variables paginacio */
if (!isset($_GET['pagina']))
   $pag_actual=1;
else
   $pag_actual=$_GET['pagina'];


$result = $mysql->query($select);
$num_contactes = $result->num_rows;
$limit_filas = 10;
$fila_inicial = ($pag_actual - 1) * $limit_filas;
$select .= " LIMIT $fila_inicial, $limit_filas";
$select = "(".$select.") ORDER BY " . $order . " " . $orderASC;
//echo $select;
$total_pag = ceil($num_contactes/$limit_filas);
/*
<th><a href="<?php echo $url ?>orderBy=contact_ID&orderASC=<?php echo $orderASC ?>">a</a>Usuari</th>*/

?>
<form action='funcions/eliminar.php' id='formContactes' name='formContactes' method='post' >
<table id ="taulaContactes" class='taulaContactes'>
    <tr class='detCont' id='detCont'>
    <th>
      <a href="<?php echo $url ?>orderBy=contact_ID&orderASC=<?php echo $orderASC ?>">
        <img  class="ordenarimg" src="./img/sort.png">
      </a>Usuari
    </th>
    <th>
      <a href="<?php echo $url ?>orderBy=contact_nom&orderASC=<?php echo $orderASC ?>">
        <img  class="ordenarimg" src="./img/sort.png">
      </a>Nom</th>
    <th COLSPAN="2">
      <a href="<?php echo $url ?>orderBy=contact_cog1&orderASC=<?php echo $orderASC ?>">
        <img  class="ordenarimg" src="./img/sort.png">
      </a>Cognoms
    </th>
    <th>
      <a href="<?php echo $url ?>orderBy=contact_edat&orderASC=<?php echo $orderASC ?>">
        <img  class="ordenarimg" src="./img/sort.png">
      </a>Naixament
    </th>
    <th>
      <a href="<?php echo $url ?>orderBy=contact_tlf&orderASC=<?php echo $orderASC ?>">
        <img  class="ordenarimg" src="./img/sort.png">
      </a>Telefon
    </th>
    <th>
      <input type='button' id="bEliminar" name="bt" value='Eliminar' disabled onclick="eliminarUsuari()" />
      <input type="checkbox" id="chkEliminar" onclick="toggle(this)" onchange="bt.disabled = !this.checked"  />
    </th>
  </tr>
  <script>
  var f = document.formContactes;
  alert(f.inp_tdNom.value);
  </script>
<?php
  $sql= $mysql->query($select);
  while($filas = $sql->fetch_array(MYSQLI_ASSOC))
  {
    echo "<tr id='trCont" . $filas["contact_ID"]. "'>";
      echo "<td class='tdId' class='campContactes'>" . $filas["contact_ID"]. "</td>";
      echo "<td class='tdNom' class='campContactes'>" . $filas["contact_nom"]."</td>";
      echo "<td class='tdCog1' class='campContactes'>".$filas["contact_cog1"]. "</td>";
      echo "<td class='tdCog2' class='campContactes'>".$filas["contact_cog2"]. "</td>";
      echo "<td class='tdEdat' class='tdEdatcampContactes'>" . $filas["contact_edat"]. "</td>";
      echo "<td class='tdTlf' class='campContactes'>" . $filas["contact_tlf"]. "</td>";
      echo "<td class='tdFuncions' class='campContactes'>
              <a class='editar' id='bEditar' onclick='editarContacte(".$filas["contact_ID"].")'>editar</a>
              <input type='checkbox' onchange='bt.disabled = !this.checked' name='checkdel[]' id='elimchk' value='" . $filas['contact_ID']. "'/>
            </td>";
    echo "</tr>";
  }

  echo "</table>";
  echo"</form>";

  /*Paginacio*/
  echo "<div class='paginacio'>";
  echo "<ul>";
  if(($pag_actual - 1) == 0)
    echo "<li><a href='#'>&laquo;</a></li>";
  else
    echo "<li><a href=".$url."pagina=".($pag_actual-1)."><b>&laquo;</b></a></li>";

  for($num_pag=1; $num_pag <= $total_pag; $num_pag++)
  {
    if($num_pag == $pag_actual)
      echo "<li><a href='#'><b> ".$num_pag." </b></a></li>";
    else
      echo "<li><a href=".$url."pagina=".$num_pag."> ".$num_pag." </a></li>";
  }

  if($total_pag == $pag_actual)
    echo "<li><a href='#'>&raquo;</a></li>";
  else
    echo "<li><a href=".$url."pagina=".($pag_actual+1)."'><b>&raquo;</b></a></li>";

  echo "</ul>";
  echo "</div>";
}
?>
