<?php
require_once('conexio.php');

/* variables sql*/
$taula = 'contactes';
$campBus = 'contact_nom';
$user_id = 0;
if (isset($_POST['user_id']))$user_id = $_POST['user_id'];
$select = "SELECT * FROM ".$taula." WHERE 'contact_ID' = ".$user_id;
//$count  ="SELECT COUNT(*) FROM ".TABLE." WHERE 'contact_ID' = ".$user_id;
$mysql = new mysqli(HOST, USER, PASSWD, DB);

/* conexión al servidor de base de datos */
if ($mysql->connect_error)
  die("Error al conectarse al servidor");
else{
  $result = $mysql->query($select);
  $num_contactes = $result->num_rows;
  //$num = $mysql->query($count);
}

//Buscador
$cadena = "";
if (isset($_POST['searchbox'])) {
  $cadena = $_POST['searchbox'];
  $select = "SELECT * FROM ".$taula."
  WHERE 'contact_ID' = ".$user_id." AND (contact_nom LIKE '%{$cadena}%'
  OR contact_cog1 LIKE '%{$cadena}%'
  OR contact_cog2 LIKE '%{$cadena}%'
  OR contact_tlf LIKE '%{$cadena}%'
  OR contact_edat LIKE '%{$cadena}%')
  ";
  $result = $mysql->query($select);
  $num_contactes = $result->num_rows;
}

  /* variables paginacio */
if (!isset($_GET['pos']))
   $pag_actual=1;
else
   $pag_actual=$_GET['pos'];

$limit_filas = 10;
$fila_inicial = ($pag_actual - 1) * $limit_filas;
$select .= " LIMIT $fila_inicial, $limit_filas";
//$n = $num->fetch_array();
$total_pag = ceil($num_contactes/$limit_filas);
//echo "count:".$num_contactes."/".$limit_filas."-total pag:".$total_pag;
$url = basename($_SERVER ["PHP_SELF"]);
?>
<table id ="taulaContactes" class='taulaContactes'>
  <tbody>
    <form id='formContactes' name='formContactes' action=''  method='post' >
      <tr class='detCont' id='detCont'>
      <th>Usuari</th>
      <th>Nom</th>
      <th>Cognoms</th>
      <th>Telefon</th>
      <th>Naixament</th>
      <th>
        <input type='button' id="bEliminar" name="bt" value='Eliminar' disabled onclick="eliminarUsuari()" />
        <input type="checkbox" onchange="bt.disabled = !this.checked" onclick="toggle(this)" />
      </th>
    </tr>
<?php
  $sql= $mysql->query($select);
  while($filas = $sql->fetch_array(MYSQLI_ASSOC))
  {
    echo "<tr id='cnt" . $filas["contact_ID"]. "'>";
      echo "<td id='cId' class='campContactes'>" . $filas["contact_ID"]. "</td>";
      echo "<td id='cNom' class='campContactes'>" . $filas["contact_nom"]."</td>";
      echo "<td id='cCog' class='campContactes'>".$filas["contact_cog1"]." ".$filas["contact_cog2"]. "</td>";
      echo "<td id='cEdat' class='tdEdatcampContactes'>" . $filas["contact_tlf"]. "</td>";
      echo "<td id='cTlf' class='campContactes'>" . $filas["contact_edat"]. "</td>";
      echo "<td id='cFuncions' class='campContactes'>
              <a class='editar' id='bEditar' onclick='editarContacte(".$filas["contact_ID"].")'>editar</a>
              <input type='checkbox' onchange='bt.disabled = !this.checked' name='checkdel[]' id='elimchk' value='" . $filas["contact_ID"]. "'
            </td>";
    echo "</tr>";
  }
  echo"</form>";
  echo "</tbody>";
  echo "</table>";

  /*Paginacio*/

  echo "<div class='paginacio'>";
  echo "<ul>";
  if(($pag_actual - 1) == 0)
    echo "<li><a href='#'>&laquo;</a></li>";
  else
    echo "<li><a href='$url?pos=".($pag_actual-1)."'><b>&laquo;</b></a></li>";

  for($num_pag=1; $num_pag <= $total_pag; $num_pag++)
  {
    if($num_pag == $pag_actual)
      echo "<li><a href='#'><b> ".$num_pag." </b></a></li>";
    else
      echo "<li><a href='$url?pos=$num_pag'> ".$num_pag." </a></li>";
  }

  if($total_pag == $pag_actual)
    echo "<li><a href='#'>&raquo;</a></li>";
  else
    echo "<li><a href='$url?pos=".($pag_actual+1)."'><b>&raquo;</b></a></li>";

  echo "</ul>";
  echo "</div>";
?>
