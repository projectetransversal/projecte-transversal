<?php
require_once('conexio.php');

/* variables */
$taula = 'contactes';
$url = basename($_SERVER ["PHP_SELF"]);
$limit_end = 15;
$init = ($ini-1) * $limit_end;
$campBus = 'contact_nom';
$str = "";

/* querys */
$count="SELECT COUNT(*) FROM ".TABLE."";
$select = "SELECT * FROM ".$taula;
if (isset($_POST['searchbox']) && $_POST['searchbox'] != "" ) {
  $str = $_POST['searchbox'];
  $select = "SELECT * FROM ".$taula."
  WHERE contact_nom LIKE '%{$str}%'
  OR contact_cog1 LIKE '%{$str}%'
  OR contact_cog2 LIKE '%{$str}%'
  OR contact_tlf LIKE '%{$str}%'
  OR contact_edat LIKE '%{$str}%'
  ";
  //echo $select;
}
$select .= " LIMIT $init, $limit_end";
// where title LIKE '%{$str}%'";

/* conexión al servidor de base de datos */
$mysql = new mysqli(HOST, USER, PASSWD, DB);

if ($mysql->connect_error)
{
  die("Error al conectarse al servidor");
}
else{
  $num = $mysql->query($count);
  $n = $num->fetch_array();
  $total = ceil($n[0]/$limit_end);
  ?>
  <form action='funcions/eliminar.php' id='deleteform' method='get' >
  <table class='Tcontactes'>
  <tr class='detCont' id='detCont'>
    <th>Usuari</th>
    <th>Nom</th>
    <th>Telefon</th>
    <th>Naixament</th>
    <th><input type='submit' value='Eliminar' /><input type="checkbox" onClick="toggle(this)" /></th>
  </tr>

<?php
  $c = $mysql->query($select);
  while($rows = $c->fetch_array(MYSQLI_ASSOC))
  {
    echo "<tr>";
    echo "<td class='tdID'>" . $rows["contact_ID"]. "</td>";
    echo "<td class='tdNom'>" . $rows["contact_nom"]." ".$rows["contact_cog1"]." ".$rows["contact_cog2"]. "</td>";
    echo "<td class='tdEdat'>" . $rows["contact_tlf"]. "</td>";
    echo "<td class='tdTlf'>" . $rows["contact_edat"]. "</td>";
    echo "<td><input type='checkbox' name='checkdel[]' id='elimchk' value='" . $rows["contact_ID"]. "'</td>";
    echo "</tr>";
  }

  echo "</tbody>";
  echo "</table>";
  echo"</form>";

  /* numeració*/
  echo "<div class='pagination'>";
  echo "<ul>";

  if(($ini - 1) == 0)
    echo "<li><a href='#'>&laquo;</a></li>";
  else
    echo "<li><a href='$url?pos=".($ini-1)."'><b>&laquo;</b></a></li>";

  for($k=1; $k <= $total; $k++)
  {
    if($ini == $k)
      echo "<li><a href='#'><b>".$k."</b></a></li>";
    else
      echo "<li><a href='$url?pos=$k'>".$k."</a></li>";
  }

  if($ini == $total)
    echo "<li><a href='#'>&raquo;</a></li>";
  else
    echo "<li><a href='$url?pos=".($ini+1)."'><b>&raquo;</b></a></li>";

  echo "</ul>";
  echo "</div>";
}
?>
