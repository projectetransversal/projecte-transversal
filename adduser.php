<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Afegir usuari</title>
	<meta charset="utf-8"/>
  <link rel="stylesheet" href="./css/global.css" />
	<link rel="stylesheet" href="./css/adduser.css" />
		<link rel="stylesheet" href="./css/calendari.css" />
	<script src="./js/adduser.js" ></script>
  <script type="text/javascript" src="./js/calendari2.js"></script>
</head>
<body>
	<h1>Afegir usuari</h1>
	<div id="afegirUsuari" >
	  <form id="formAfegir" method="POST" action="funcions/insertar_usuari.php" name="classm" >
		  <p><label class="nom" >Nom</label>
				<input type="text" id="nom" class="inpAfegir" name="nom" placeholder="Nom" maxlength="15" />
			</p>
		  <p><label class="cognom1" >Primer Cognom</label>
				<input type="text" id="cognom1" class="inpAfegir" name="cognom1" placeholder="Primer Cognom" maxlength="15" />
			</p>
		  <p><label class="cognom2" >Segon Cognom</label>
				<input type="text" id="cognom2" class="inpAfegir" name="cognom2" placeholder="Segon Cognom" maxlength="15" /></p>
		  <p id="edat"><label class="naixament" >Data Naixament</label>
				<input type="text" id="inp_tdEdat" class="inpAfegir" name="datanaixament" placeholder="Edat"  onclick="inicial()" maxlength="10" />
			<a href="javascript:calendari()"><img width="25" height="25" src="img/agenda.png"/></a>
			</p>
		  <p><label class="telefon" >Telefon</label>
				<input type="text" id="telefon" class="inpAfegir" name="telefon" placeholder="Telefon"  maxlength="9"/>
			</p>
	    <p class="btnAfegir">
				<input type="button" id="btnInsertar "class="btn" value="Afegir" onclick="insertarUsuari()" />
	    	<input type="button" id="bSortir" class="btn" value="Cancelar" onclick="location.href='agenda.php';"/>
			</p>
	  </form>
	</div>
</body>
</html>
