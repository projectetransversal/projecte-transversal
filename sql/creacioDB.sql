CREATE DATABASE agenda;
--
-- Estructura de la taula `contactes`
--
CREATE TABLE IF NOT EXISTS `agenda`.`contactes` (
  `contact_ID` int(11) NOT NULL AUTO_INCREMENT,
  `user_ID` int(11) NOT NULL,
  `contact_nom` varchar(30) NOT NULL,
  `contact_cog1` varchar(30) NOT NULL,
  `contact_cog2` varchar(30) NOT NULL,
  `contact_edat` date NOT NULL,
  `contact_tlf` int(9) NOT NULL,
  PRIMARY KEY (`contact_ID`),
  KEY `user_id` (`user_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=394 ;
--
-- Estructura de la taula `user`
--

CREATE TABLE IF NOT EXISTS `agenda`.`user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `user_login` varchar(16) NOT NULL,
  `user_pass` varchar(16) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;


--
-- Restriccions per la taula `contactes`
--
ALTER TABLE `agenda`.`contactes`
  ADD CONSTRAINT `contactes_ibfk_2` FOREIGN KEY (`user_ID`) REFERENCES `user` (`ID`),
  ADD CONSTRAINT `contactes_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`ID`);
