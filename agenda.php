<?php
$a= session_id();
if(empty($a)){
  session_start();
  $userID=session_id();
}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Agenda</title>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="./css/global.css" />
  <link rel="stylesheet" href="./css/contactes.css" />
  <link rel="stylesheet" href="./css/calendari.css" />
  <script type="text/javascript" src="./js/calendari2.js"></script>
  <script type="text/javascript" src="./js/validar.js"></script>
  <script language="JavaScript">

  //funcio de confirmacio si es vol eliminar contacte
  function eliminarUsuari(){
      if(confirm("Eliminar els contactes seleccionats?")){
        formContactes =  document.getElementById('formContactes');
        formContactes.submit();
      }
  }

  function toggle(source) {
      checkboxes = document.getElementsByName('checkdel[]');
      for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = source.checked;
      }
    }
</script>
</head>
<body>
<header>
  <h1>Llibreta de contactes <?php //echo $_SESSION['usuario'];?></h1>
</header>

<section>
  <div id="nav">

      <a class="addCont" href="adduser.php">
        <img src="./img/agenda/afegir.png">
        <span >Afegir usuari</span>
      </a>

    <!--<p class="filtrCont">Aplicar filtre</p> <a href=""><img id="buscaicono" src="img/buscador.png"/></a>-->
    <form class="cercaBox" action="" method="get">
     <input type="text" id="inpBuscar" name="searchbox"  value="<?php if(isset($_GET['searchbox'])) echo $_GET["searchbox"];?>"  class="cercaInp" placeholder="Cerca..." />

    </form>

    <p class="numCont">
      <img src="./img/agenda/nContactes.png">
      <span id="totalContactes"><?php echo "contactes";  ?></span>
    </p>
  </div>
  <div id="contactes">
     <?php include('funcions/llistaContactes.php'); ?>
     <script type="text/javascript" src="./js/editar.js"></script>
  </div>
  <p><input type="button" id="bSortir" onclick="location.href='funcions/sortir.php';" value="Sortir" /></p>
</section>
</body>
</html>
